package com.dougmccluer.mycrew.ui.screens.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dougmccluer.mycrew.data.repository.IFavoritesRepository
import com.dougmccluer.mycrew.data.repository.IStarWarsRepository
import com.dougmccluer.mycrew.logging.Clog
import com.dougmccluer.mycrew.ui.viewmodel.PersonSharedViewModel
import com.dougmccluer.podrick.event.EventDispatcher
import com.dougmccluer.podrick.event.EventStream
import com.dougmccluer.podrick.event.dispatch
import com.squareup.moshi.Moshi
import com.swapi.models.SwapiPerson
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable

class SearchViewModel(
	private val starWarsRepository:IStarWarsRepository,
	val favoritesRepository:IFavoritesRepository,
	private val personSharedViewModel:PersonSharedViewModel
):ViewModel() {

	private val _showDetailEvent = EventDispatcher<Any>()
	/** This event fires when a user clicks on a person in the list **/
	val showDetailEvent:EventStream<Any> = _showDetailEvent

	val onPersonClicked : (SwapiPerson)->Unit = {
		personSharedViewModel.personLiveData.value = it
		_showDetailEvent.dispatch(Any())
	}

	private var people:List<SwapiPerson> = listOf()
		set(value) {
			field = value
			_peopleLiveData.value = value
		}

	private val _queryTextLiveData = MutableLiveData<String>()
	val queryTextLiveData:LiveData<String> = _queryTextLiveData

	private val _peopleLiveData = MutableLiveData<List<SwapiPerson>>()
	val peopleLiveData:LiveData<List<SwapiPerson>> = _peopleLiveData

	fun onActivityStarted() {
		if(people.isEmpty()) {
			fetchPeople()
		}
	}

	fun onQuerySubmitted(query:String?):Boolean {
		return true
	}

	fun onQueryTextChanged(newText:String?):Boolean {
		if(newText == _queryTextLiveData.value) {
			return true
		}
		fetchPeople(newText)
		return true
	}


	private fun fetchPeople(searchString:String? = null) {
		starWarsRepository.getPeople(searchString)
			.subscribe(object:SingleObserver<List<SwapiPerson>> {
				override fun onSubscribe(d:Disposable) {}

				override fun onSuccess(response:List<SwapiPerson>) {
					people = response
				}

				override fun onError(tr:Throwable) {
					//TODO: show error message to the user
					Clog.e(msg = "call failed", throwable = tr)
				}
			})
	}

}
