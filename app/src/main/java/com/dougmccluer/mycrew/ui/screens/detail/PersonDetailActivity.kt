package com.dougmccluer.mycrew.ui.screens.detail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import androidx.core.view.isVisible
import com.dougmccluer.mycrew.databinding.PersonDetailActivityBinding
import org.koin.android.viewmodel.ext.android.viewModel

class PersonDetailActivity:AppCompatActivity() {

	private val viewModel:PersonDetailViewModel by viewModel()
	lateinit var binding:PersonDetailActivityBinding

	override fun onCreate(savedInstanceState:Bundle?) {
		super.onCreate(savedInstanceState)
		binding = PersonDetailActivityBinding.inflate(LayoutInflater.from(this))
		setContentView(binding.root)

		binding.apply {
			detailAddFavoriteBtn.setOnClickListener {
				viewModel.onAddFavoriteClicked()
			}

			detailRemoveFavoriteBtn.setOnClickListener {
				viewModel.onRemoveFavoriteClicked()
			}
		}

		viewModel.isFavoriteLiveData.observe(this){
			binding.detailAddFavoriteBtn.isVisible = !it
			binding.detailRemoveFavoriteBtn.isVisible = it
		}

		viewModel.nameLiveData.observe(this){
			binding.detailNameDisplay.text = it
		}

		viewModel.birthyearLiveData.observe(this){
			binding.detailBirthyearDisplay.text = it
		}

		viewModel.heightLiveData.observe(this){
			binding.detailHeightDisplay.text =it
		}
		viewModel.massLiveData.observe(this){
			binding.detailMassDisplay.text = it
		}
		viewModel.haircolorLiveData.observe(this){
			binding.detailHaircolorDisplay.text = it
		}
		viewModel.eyecolorLiveData.observe(this){
			binding.detailEyecolorDisplay.text = it
		}
		viewModel.skincolorLiveData.observe(this){
			binding.detailSkincolorDisplay.text = it
		}
		viewModel.genderLiveData.observe(this){
			binding.detailGenderDisplay.text =it
		}
	}

	override fun onResume() {
		super.onResume()
		viewModel.onActivityResumed()
	}

	override fun onPause() {
		super.onPause()
		viewModel.onActivityPaused()
	}

	companion object{
		const val EXTRA_PERSON:String = "EXTRA_PERSON"
	}
}