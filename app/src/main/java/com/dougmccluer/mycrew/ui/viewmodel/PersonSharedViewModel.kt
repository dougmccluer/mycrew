package com.dougmccluer.mycrew.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import com.swapi.models.SwapiPerson

class PersonSharedViewModel {
	var personLiveData=MutableLiveData<SwapiPerson>()
}