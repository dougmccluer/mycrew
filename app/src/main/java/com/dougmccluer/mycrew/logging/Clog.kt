package com.dougmccluer.mycrew.logging

import android.util.Log
import com.dougmccluer.mycrew.BuildConfig
import java.io.PrintWriter
import java.io.StringWriter

object Clog {

	private val buildTypes = arrayOf("debug")
	private val remoteTargets:ArrayList<RemoteLogger> = arrayListOf()

	const val LOG_TAG:String = "-=[MyCrew]=-"
	const val DEBUG = "DEBUG"
	const val WARN = "WARNING"
	const val ERROR = "ERROR"

	interface RemoteLogger {
		val name:String
		fun log(msg:String, tr:Throwable?, level:String = DEBUG)
	}

	/** adds a remote logging target.  Note: does not check for redundant entries.  If you want to
	 * avoid redundancy, call removeRemoteLogger(name) first*/
	fun addRemoteLogger(target:RemoteLogger) {
		remoteTargets.add(target)
	}

	/** removes all targets with the supplied name */
	fun removeRemoteLogger(name:String) {
		remoteTargets.removeAll(remoteTargets.filter { it.name == name })
	}


	fun log(
		msg:String,
		level:String = DEBUG,
		remote:Boolean = false,
		throwable:Throwable? = null,
		tag:String? = null
	) {
		val logTag = tag ?: LOG_TAG
		if(remote && !BuildConfig.DEBUG) {
			var sw = StringWriter()
			var t:Throwable = throwable ?: Throwable()
			t.printStackTrace(PrintWriter(sw))
			try {

			} catch(tr:Throwable) {
				Log.d(logTag, "failed to send remote log")
			}
		}

		if(BuildConfig.BUILD_TYPE in buildTypes && BuildConfig.DEBUG) {
			when(level) {
				DEBUG -> throwable?.let { Log.d(logTag, msg, throwable) } ?: let {
					Log.d(logTag, msg)
				}
				WARN -> throwable?.let { Log.w(logTag, msg, throwable) } ?: let {
					Log.w(logTag, msg)
				}
				ERROR -> throwable?.let { Log.e(logTag, msg, throwable) } ?: let {
					Log.e(logTag, msg)
				}
			}
		}
	}

	fun e(
		msg:String,
		throwable:Throwable? = null,
		remote:Boolean = false,
		tag:String? = null
	) {
		log(msg, ERROR, remote, throwable)
	}
}