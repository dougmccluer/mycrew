plugins {
	id("java-library")
	id ("kotlin")
	kotlin("kapt")
}

java {
	sourceCompatibility = JavaVersion.VERSION_1_8
	targetCompatibility = JavaVersion.VERSION_1_8
}

/**
 * Hey, what's with these dependency declarations?  Where's the version number?
 *
 * This project uses "Gradle refreshVersions" (see: https://github.com/jmfayard/refreshVersions/) to
 * sync dependency versions across modules.  The version numbers are defined in the
 * "versions.properties" file in the root project.
 */

dependencies {
	implementation("org.jetbrains.kotlin:kotlin-stdlib:_")

	//Moshi - json serialization
	implementation("com.squareup.moshi:moshi:_")
	implementation("com.squareup.moshi:moshi-kotlin:_")
	implementation("com.squareup.moshi:moshi-adapters:_")
	kapt("com.squareup.moshi:moshi-kotlin-codegen:_")

	//OKHTTP & Retrofit - HTTP request management
	implementation("com.squareup.okhttp3:okhttp:_")
	implementation("com.squareup.okhttp3:logging-interceptor:_")
	implementation("com.squareup.retrofit2:retrofit:_")
	implementation("com.squareup.retrofit2:adapter-rxjava2:_")
	implementation("com.squareup.retrofit2:converter-moshi:_")

	//Rx - asynchronous observable streams
	implementation("io.reactivex.rxjava2:rxjava:_")
	implementation("io.reactivex.rxjava2:rxandroid:_")
}