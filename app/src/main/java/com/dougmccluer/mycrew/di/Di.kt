package com.dougmccluer.mycrew.di

import android.content.Context
import android.content.SharedPreferences
import com.dougmccluer.mycrew.data.repository.FavoritesRepository
import com.dougmccluer.mycrew.data.repository.IFavoritesRepository
import com.dougmccluer.mycrew.data.repository.IStarWarsRepository
import com.dougmccluer.mycrew.data.repository.StarWarsRepository
import com.dougmccluer.mycrew.data.tools.MyCrewHttpLogger
import com.dougmccluer.mycrew.data.tools.TypesAdapterFactory
import com.dougmccluer.mycrew.data.tools.XNullableAdapterFactory
import com.dougmccluer.mycrew.ui.screens.detail.PersonDetailViewModel
import com.dougmccluer.mycrew.ui.screens.search.SearchViewModel
import com.dougmccluer.mycrew.ui.viewmodel.PersonSharedViewModel
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import com.swapi.sw.StarWarsAPI
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

object Di {

	fun modules() = listOf(repositoryModule, viewModelModule, app_module)

	/**
	 * As apps grow, the number of injected dependencies can get quite large, and it becomes helpful
	 * to split up the list into separate modules in separate files.  For now the app is small so we
	 * define all the modules in this file, but still organize them into separate modules which will
	 * be easy to move into other files in the future
	 */
	private val viewModelModule = module {
		viewModel { SearchViewModel(
			starWarsRepository = get(),
			favoritesRepository = get(),
			personSharedViewModel = get())
		}
		viewModel { PersonDetailViewModel(personSharedViewModel = get(),favoritesRepository = get()) }

		single{ PersonSharedViewModel() }
	}

	private val app_module = module {
		single<SharedPreferences> {
			androidContext().getSharedPreferences("pref", Context.MODE_PRIVATE)
		}
	}

	private val repositoryModule = module {
		single<IStarWarsRepository> { StarWarsRepository(get()) }
		single<IFavoritesRepository> { FavoritesRepository(get()) }

		single { createOkHttpClient() }
		single<Moshi> {
			Moshi.Builder()
				.add(XNullableAdapterFactory())
				.add(KotlinJsonAdapterFactory())
				.add(TypesAdapterFactory())
				.build()
		}
		single {
			createWebService<StarWarsAPI>(
				okHttpClient = get(),
				"https://swapi.dev/api/",
				moshi = get()
			)
		}
	}

	private fun createOkHttpClient():OkHttpClient {
		val loggingInterceptor =
			HttpLoggingInterceptor(MyCrewHttpLogger())
				.setLevel(HttpLoggingInterceptor.Level.BODY)

		return OkHttpClient.Builder()
			.readTimeout(60, TimeUnit.SECONDS)
			.connectTimeout(120, TimeUnit.SECONDS)
			.addInterceptor(loggingInterceptor)
			.build()
	}

	private inline fun <reified T> createWebService(
		okHttpClient:OkHttpClient,
		url:String,
		moshi:Moshi
	):T {
		val retrofit = Retrofit.Builder()
			.baseUrl(url)
			.client(okHttpClient)
			.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
			.addConverterFactory(
				MoshiConverterFactory.create(moshi).asLenient().withNullSerialization()
			)
			.build()
		return retrofit.create(T::class.java)
	}


}
