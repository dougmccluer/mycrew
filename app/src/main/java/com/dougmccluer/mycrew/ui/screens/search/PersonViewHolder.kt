package com.dougmccluer.mycrew.ui.screens.search

import android.view.View
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.dougmccluer.mycrew.R
import com.dougmccluer.mycrew.databinding.PersonViewHolderBinding
import com.dougmccluer.mycrew.ui.utils.ImageFactory
import com.swapi.models.SwapiPerson

class PersonViewHolder(view:View):RecyclerView.ViewHolder(view) {

	fun bind(
		person:SwapiPerson,
		imageFactory:ImageFactory,
		isFavorite:Boolean = false,
		onFavoriteClicked:()->Unit,
		onUnfavoriteClicked:()->Unit,
		onSelected:()->Unit
	) {
		PersonViewHolderBinding.bind(itemView).apply {
			nameDisplay.text = person.name

			btnAddFavorite.isVisible = !isFavorite
			btnAddFavorite.setOnClickListener {
				onFavoriteClicked()
			}

			btnRemoveFavorite.isVisible = isFavorite
			btnRemoveFavorite.setOnClickListener {
				onUnfavoriteClicked()
			}

			root.setOnClickListener {
				onSelected()
			}

			itemView.context.resources.getDimension(R.dimen.thumbnail_size).let { thumbSize ->
				initialsImage.setImageDrawable(
					imageFactory.generateInitialsDrawable(
						text = person.name,
						radius = thumbSize.toInt(),
						textPxSize = (thumbSize * 0.5).toInt()
					)
				)
			}
		}
	}

}