package com.dougmccluer.mycrew.data.repository

import io.reactivex.Single

interface IFavoritesRepository {
	/** @return true if the named person is currently a favorite, false otherwise**/
	fun checkFavoriteStatus(name:String):Boolean
	/** @return true if the action was successful, false otherwise**/
	fun setAsFavorite(name:String):Boolean
	/** @return true if the action was successful, false otherwise**/
	fun removeFavorite(it:String):Boolean

	companion object{
		const val FAVORITES_KEY:String = "favorites"
	}
}