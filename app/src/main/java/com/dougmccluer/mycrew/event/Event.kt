package com.dougmccluer.podrick.event

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer

/**
 * Used as a wrapper for data that is exposed via a LiveData that represents an event.
 * https://medium.com/androiddevelopers/livedata-with-snackbar-navigation-and-other-events-the-singleliveevent-case-ac2622673150
 */
open class Event<out T>(private val content:T) {

	var hasBeenHandled = false
		private set // Allow external read but not write

	/**
	 * Returns the content and prevents its use again.
	 */
	fun getEventIfNotHandled():T? {
		return if(hasBeenHandled) {
			null
		} else {
			hasBeenHandled = true
			content
		}
	}

	/**
	 * Returns the content, even if it's already been handled.
	 */
	fun peekContent():T = content
}

typealias EventStream<T> = LiveData<Event<T>>
typealias EventDispatcher<T> = MutableLiveData<Event<T>>

/** convenience method, equivalent to <code>dispatcher.value = Event(data)</code> **/
fun <T> EventDispatcher<T>.dispatch(data:T) {
	this.value = Event(data)
}

/** convenience method, equivalent to <code>dispatcher.value = Event(Unit)</code> **/
fun EventDispatcher<Unit>.dispatch() {
	this.value = Event(Unit)
}

fun <T> LifecycleOwner.observeEvent(eventLiveData:EventStream<T>, handleEvent:(T) -> Unit) {
	eventLiveData.observe(this, Observer { e ->
		e?.getEventIfNotHandled()?.let { handleEvent(it) }
	})
}