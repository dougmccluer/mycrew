package com.dougmccluer.mycrew.extensions

import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable

fun <T> Single<T>.addSubscriber(
	onSuccess:(T) -> Unit,
	onFail:((Throwable) -> Unit)? = null
) {
	this.subscribe(object:SingleObserver<T> {

		override fun onSuccess(response:T) {
			onSuccess.invoke(response)
		}

		override fun onSubscribe(d:Disposable) {
		}

		override fun onError(e:Throwable) {
			onFail?.invoke(e) ?: run {
				//TODO: add generic error handler
			}
		}
	})
}