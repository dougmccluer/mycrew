package com.dougmccluer.mycrew.data.repository

import android.content.SharedPreferences
import io.reactivex.Single

class FavoritesRepository(
	private val sharedPreferences:SharedPreferences
):IFavoritesRepository {

	private fun currentFavs() = sharedPreferences.getStringSet(
		IFavoritesRepository.FAVORITES_KEY,
		setOf<String>()
	)?: setOf()

	override fun checkFavoriteStatus(name:String):Boolean {
		return sharedPreferences.getStringSet(
			IFavoritesRepository.FAVORITES_KEY,
			setOf<String>()
		)?.contains(name) ?: false
	}

	/**
	 *
	 */
	override fun removeFavorite(name:String):Boolean {
		try {
			currentFavs().let {
				if(it.contains(name)) {
					sharedPreferences.edit().putStringSet(
						IFavoritesRepository.FAVORITES_KEY,
						mutableSetOf<String>().apply {
							addAll(it)
							remove(name)
						}
					).apply()
				}
			}
			return true
		}catch(tr:Throwable){
			return false
		}
	}

	override fun setAsFavorite(name:String):Boolean {
		try {
			currentFavs().let {
				if(!it.contains(name)) {
					sharedPreferences.edit().putStringSet(
						IFavoritesRepository.FAVORITES_KEY,
						mutableSetOf<String>().apply {
							addAll(it)
							add(name)
						}
					).apply()
				}
			}
			return true
		}catch(tr:Throwable){
			return false
		}
	}

}