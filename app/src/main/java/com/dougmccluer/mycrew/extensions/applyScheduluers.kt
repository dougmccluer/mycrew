package com.dougmccluer.mycrew.extensions

import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

fun <T:Any?> Single<T>.applySchedulers(
	subscribeScheduler:Scheduler = Schedulers.io(),
	observeScheduler:Scheduler = AndroidSchedulers.mainThread()
) = subscribeOn(subscribeScheduler).observeOn(observeScheduler)