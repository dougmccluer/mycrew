plugins {
	id("com.android.application")
	kotlin("android")
	kotlin("kapt")
}


android {
	compileSdkVersion(30)
	buildToolsVersion = "30.0.2"

	buildFeatures {
		viewBinding = true
	}
	defaultConfig {
		applicationId = "com.dougmccluer.mycrew"
		minSdkVersion(21)
		targetSdkVersion(30)
		versionCode = 1
		versionName = "1.0"
		testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
	}

	buildTypes {
		named("release").configure {
			isMinifyEnabled = false
			proguardFiles(
				getDefaultProguardFile("proguard-android-optimize.txt"),
				"proguard-rules.pro"
			)
		}
	}

	compileOptions {
		sourceCompatibility = JavaVersion.VERSION_1_8
		targetCompatibility = JavaVersion.VERSION_1_8
		isCoreLibraryDesugaringEnabled = true
	}

	kotlinOptions {
		jvmTarget = "1.8"
		languageVersion = "1.4"
		useIR = false
	}
}

/**
 * Hey, what's with these dependency declarations?  Where's the version number?
 *
 * This project uses "Gradle refreshVersions" (see: https://github.com/jmfayard/refreshVersions/) to
 * sync dependency versions across modules.  The version numbers are defined in the
 * "versions.properties" file in the root project.
 */

dependencies {

	implementation(project(":swapi"))

	coreLibraryDesugaring( "com.android.tools:desugar_jdk_libs:_")

	implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:_")
	implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:_")
	implementation("org.jetbrains.kotlinx:kotlinx-coroutines-rx2:_")

	implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:_")
	implementation("androidx.lifecycle:lifecycle-viewmodel-savedstate:_")
	implementation("androidx.lifecycle:lifecycle-runtime-ktx:_")

	implementation("androidx.activity:activity-ktx:_")

//koin - dependency injection
	implementation("org.koin:koin-core:_")
	implementation("org.koin:koin-androidx-scope:_")
	implementation("org.koin:koin-android-viewmodel:_")

	//Moshi - json serialization
	implementation("com.squareup.moshi:moshi:_")
	implementation("com.squareup.moshi:moshi-kotlin:_")
	implementation("com.squareup.moshi:moshi-adapters:_")
	kapt("com.squareup.moshi:moshi-kotlin-codegen:_")

	//OKHTTP & Retrofit - HTTP request management
	implementation("com.squareup.okhttp3:okhttp:_")
	implementation("com.squareup.okhttp3:logging-interceptor:_")
	implementation("com.squareup.retrofit2:retrofit:_")
	implementation("com.squareup.retrofit2:adapter-rxjava2:_")
	implementation("com.squareup.retrofit2:converter-moshi:_")

	//Rx - asynchronous observable streams
	implementation("io.reactivex.rxjava2:rxjava:_")
	implementation("io.reactivex.rxjava2:rxandroid:_")

	//Glide - image loading & caching
	implementation("com.github.bumptech.glide:glide:_")
	kapt("com.github.bumptech.glide:compiler:_")
	kapt("androidx.lifecycle:lifecycle-common-java8:_")

	implementation("org.jetbrains.kotlin:kotlin-stdlib:_")
	implementation("androidx.core:core-ktx:_")
	implementation("androidx.appcompat:appcompat:_")
	implementation("com.google.android.material:material:_")
	implementation("androidx.constraintlayout:constraintlayout:_")

	testImplementation("junit:junit:_")

	androidTestImplementation("androidx.test.ext:junit:_")
	androidTestImplementation("androidx.test.espresso:espresso-core:_")

}