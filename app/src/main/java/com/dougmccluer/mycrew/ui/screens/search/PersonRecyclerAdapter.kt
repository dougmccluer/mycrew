package com.dougmccluer.mycrew.ui.screens.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dougmccluer.mycrew.R
import com.dougmccluer.mycrew.data.repository.IFavoritesRepository
import com.dougmccluer.mycrew.ui.utils.ImageFactory
import com.swapi.models.SwapiPerson

class PersonRecyclerAdapter(
	private val imageFactory:ImageFactory,
	private val favoritesRepository:IFavoritesRepository,
	private val onPersonSelected:(SwapiPerson)->Unit
):RecyclerView.Adapter<PersonViewHolder>() {

	private val people=ArrayList<SwapiPerson>()

	fun setPeople(peeps:List<SwapiPerson>){
		people.clear()
		people.addAll(peeps)
		notifyDataSetChanged()
	}

	override fun onCreateViewHolder(parent:ViewGroup, viewType:Int):PersonViewHolder {
		return PersonViewHolder(
			LayoutInflater.from(parent.context).inflate(
				R.layout.person_view_holder,
				parent,
				false)
		)
	}

	override fun onBindViewHolder(holder:PersonViewHolder, position:Int) {
		holder.bind(
			person = people[position],
			imageFactory = imageFactory,
			isFavorite = people[position].name?.let{
				favoritesRepository.checkFavoriteStatus(it)
			} ?: false,
			onFavoriteClicked = {
				people[position].name?.let{
					favoritesRepository.setAsFavorite(it)
				}
				notifyItemChanged(position)
			},
			onUnfavoriteClicked = {
				people[position].name?.let{
					favoritesRepository.removeFavorite(it)
					notifyItemChanged(position)
				}
			},
			onSelected = {
				onPersonSelected(people[position])
			}
		)
	}

	override fun getItemCount():Int = people.size
}