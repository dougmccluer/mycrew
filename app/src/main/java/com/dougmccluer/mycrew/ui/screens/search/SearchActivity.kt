package com.dougmccluer.mycrew.ui.screens.search

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.dougmccluer.mycrew.databinding.SearchActivityBinding
import com.dougmccluer.mycrew.ui.screens.detail.PersonDetailActivity
import com.dougmccluer.mycrew.ui.utils.ImageFactory
import com.dougmccluer.podrick.event.observeEvent
import org.koin.android.viewmodel.ext.android.viewModel

class SearchActivity:AppCompatActivity() {

	private val viewModel:SearchViewModel by viewModel()
	private lateinit var personAdapter:PersonRecyclerAdapter
	private lateinit var binding:SearchActivityBinding

	override fun onCreate(savedInstanceState:Bundle?) {
		super.onCreate(savedInstanceState)
		personAdapter = PersonRecyclerAdapter(
			ImageFactory,
			viewModel.favoritesRepository,
			viewModel.onPersonClicked
		)

		binding = SearchActivityBinding.inflate(LayoutInflater.from(this))
		setContentView(binding.root)

		binding.peopleList.layoutManager = LinearLayoutManager(
			this,
			LinearLayoutManager.VERTICAL,
			false
		)
		binding.peopleList.adapter = personAdapter
	}

	override fun onStart() {
		super.onStart()

		viewModel.queryTextLiveData.observe(this, Observer { text ->
			if(text != binding.toolbar.toolbarSearch.query) {
				binding.toolbar.toolbarSearch.setQuery(text, false)
			}
		})

		viewModel.peopleLiveData.observe(this, Observer { list ->
			personAdapter.setPeople(list)
		})

		observeEvent(viewModel.showDetailEvent) { json ->
			startActivity(
				Intent(this, PersonDetailActivity::class.java)
			)
		}

		binding.toolbar.toolbarSearch.apply {
			setOnQueryTextListener(
				object:SearchView.OnQueryTextListener {
					override fun onQueryTextSubmit(query:String?):Boolean {
						(context.getSystemService(INPUT_METHOD_SERVICE) as? InputMethodManager)?.let {
							it.hideSoftInputFromWindow(windowToken, 0)
						}
						val ret = viewModel.onQuerySubmitted(query)
						setQuery("", false)
						clearFocus()
						return ret
					}

					override fun onQueryTextChange(newText:String?):Boolean {
						return viewModel.onQueryTextChanged(newText)
					}
				}
			)
		}

		viewModel.onActivityStarted()
	}
}