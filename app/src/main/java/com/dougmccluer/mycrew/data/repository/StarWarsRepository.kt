package com.dougmccluer.mycrew.data.repository

import com.dougmccluer.mycrew.extensions.applySchedulers
import com.swapi.models.SwapiPerson
import com.swapi.sw.StarWarsAPI
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class StarWarsRepository(
	private val api:StarWarsAPI,
 ):IStarWarsRepository {

	/** TODO: cache results in local database for offline use.
	 * This API is not updated frequently, and for this use case, it's not critical that we have
	 * current data before allowing the user to interact with the app, so fetching from local
	 * storage first would reduce latency
	 */
	override fun getPeople(searchString:String?):Single< List<SwapiPerson> >{
		return api.getPeople(searchString)
			.applySchedulers()
			.map { it.results }
	}

}