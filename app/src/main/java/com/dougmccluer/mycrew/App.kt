package com.dougmccluer.mycrew

import android.app.Application
import com.dougmccluer.mycrew.di.Di
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.KoinApplication
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class App:Application() {

	private var koinApp:KoinApplication? = null

	override fun onCreate() {
		super.onCreate()
		koinApp ?: let {
			koinApp = startKoin {
				//TODO: upgrade koin to latest version and remove Level.NONE
				androidLogger(Level.NONE)
				androidContext(this@App)
				modules(
					Di.modules()
				)
			}
		}
	}
}