package com.dougmccluer.mycrew.data.tools

import com.dougmccluer.mycrew.logging.Clog
import okhttp3.logging.HttpLoggingInterceptor
import org.json.JSONObject

class MyCrewHttpLogger:HttpLoggingInterceptor.Logger {

	override fun log(message:String) {
		val logName = "OkHttp"
		if(!message.startsWith("{")) {
			Clog.log(tag = logName, msg = message, remote = false)
			return
		}
		try {
			val prettyPrintJson:String =
				JSONObject(message).toString(4)
			Clog.log(tag = logName, msg = prettyPrintJson, remote = false)
		} catch(tr:Throwable) {
			Clog.log(tag = logName, msg = message, remote = false)
		}
	}
}