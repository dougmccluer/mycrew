package com.swapi.models

data class PeopleResponse (
	val count:Int,
	val next:String?,
	val previous:String?,
	val results: List<SwapiPerson>
)
