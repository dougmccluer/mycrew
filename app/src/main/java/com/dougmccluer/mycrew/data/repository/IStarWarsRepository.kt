package com.dougmccluer.mycrew.data.repository

import com.swapi.models.SwapiPerson
import io.reactivex.Single

interface IStarWarsRepository {
	fun getPeople(searchString:String?=null):Single<List<SwapiPerson>>
}