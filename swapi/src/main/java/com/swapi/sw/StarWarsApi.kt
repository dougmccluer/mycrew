package com.swapi.sw

import com.swapi.models.*
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface StarWarsAPI {

	@GET("people/")
	fun getPeople(
		@Query("search") searchString:String?=null
	):Single<PeopleResponse>

	@GET("people/{id}/")
	fun getPerson(
		@Path("id") peopleId:Int
	):Single<SwapiPerson?>


}