package com.dougmccluer.mycrew.ui.utils

import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import com.amulyakhare.textdrawable.TextDrawable

object ImageFactory {

	/**
	 * given a string of text (usually a name), creates a drawable displaying the first letters
	 * of the first two words in the string, against a rectangle with rounded corners.
	 *
	 * If no text is provided, letters will be chosen at random
	 *
	 * @param text
	 * @param radius radius of the rounded corners on the rectangle
	 * @param colors an array of colors from which one will be chosen at random.  To force a
	 * specific color, provide an array with only one item
	 * @param textPxSize
	 * @param textColor defaults to white
	 * @param font
	 */
	fun generateInitialsDrawable(
		text:String?,
		radius:Int = 20,
		colors:Array<Int>? = null,
		textPxSize:Int = 80,
		textColor:Int = Color.WHITE,
		font:Typeface = Typeface.SANS_SERIF
	):Drawable {
		var initials:String? = null
		text?.let {
			val words = it.split(" ")
			if(words.size > 1 && words[0].isNotBlank() && words[1].isNotBlank()) {
				initials = "${words[0].first()}${words[1].first()}"
			} else if(it.length > 1) {
				initials = it.substring(0, 2)
			}
		}

		//choose random letters if initials could not be extracted from the input
		if(initials == null) {
			initials = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray().let {
				"${it.random()}${it.random()}"
			}
		}

		//pick a color based upon the contents of the string
		val score:Int = text?.fold(0){ acc, char ->
			acc + char.toInt()
		} ?: 0

		val bgColor:Int = (
			colors ?: arrayOf(
				0xFFC62828,
				0xFFAD1457,
				0xFF6A1B9A,
				0xFF4527A0,
				0xFF283593,
				0xFF0277BD,
				0xFF1565C0,
				0xFF00838F,
				0xFF00695C
			)
		).let {
				val index = if(score < it.size) { score } else { score % it.size }
				it[index]
		}.toInt()

		return TextDrawable.builder().beginConfig()
			.textColor(textColor)
			.useFont(font)
			.fontSize(textPxSize)
			.endConfig()
			.buildRoundRect(initials!!, bgColor, radius)
	}
}