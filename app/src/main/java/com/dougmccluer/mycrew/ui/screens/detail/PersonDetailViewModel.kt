package com.dougmccluer.mycrew.ui.screens.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.dougmccluer.mycrew.data.repository.IFavoritesRepository
import com.dougmccluer.mycrew.ui.viewmodel.PersonSharedViewModel
import com.swapi.models.SwapiPerson

class PersonDetailViewModel(
	private val personSharedViewModel:PersonSharedViewModel,
	private val favoritesRepository:IFavoritesRepository
):ViewModel() {

	private val _isFavoriteLiveData = MutableLiveData<Boolean>()
	val isFavoriteLiveData:LiveData<Boolean> = _isFavoriteLiveData

	private val _nameLiveDataLiveData = MutableLiveData<String>()
	val nameLiveData:LiveData<String> = _nameLiveDataLiveData

	private val _heightLiveDataLiveData = MutableLiveData<String>()
	val heightLiveData:LiveData<String> = _heightLiveDataLiveData

	private val _massLiveDataLiveData = MutableLiveData<String>()
	val massLiveData:LiveData<String> = _massLiveDataLiveData

	private val _haircolorLiveDataLiveData = MutableLiveData<String>()
	val haircolorLiveData:LiveData<String> = _haircolorLiveDataLiveData

	private val _eyecolorLiveDataLiveData = MutableLiveData<String>()
	val eyecolorLiveData:LiveData<String> = _eyecolorLiveDataLiveData

	private val _skincolorLiveDataLiveData = MutableLiveData<String>()
	val skincolorLiveData:LiveData<String> = _skincolorLiveDataLiveData

	private val _birthyearLiveDataLiveData = MutableLiveData<String>()
	val birthyearLiveData:LiveData<String> = _birthyearLiveDataLiveData

	private val _genderLiveDataLiveData = MutableLiveData<String>()
	val genderLiveData:LiveData<String> = _genderLiveDataLiveData

	fun onRemoveFavoriteClicked() {
		_nameLiveDataLiveData.value?.let {
			favoritesRepository.removeFavorite(it)
			_isFavoriteLiveData.value = favoritesRepository.checkFavoriteStatus(it)
		}
	}

	fun onAddFavoriteClicked() {
		_nameLiveDataLiveData.value?.let {
			favoritesRepository.setAsFavorite(it)
			_isFavoriteLiveData.value = favoritesRepository.checkFavoriteStatus(it)
		}
	}

	fun onActivityResumed() {
		personSharedViewModel.personLiveData.observeForever(personObserver)
	}

	fun onActivityPaused() {
		personSharedViewModel.personLiveData.removeObserver(personObserver)
	}

	val personObserver = Observer<SwapiPerson> {
		_nameLiveDataLiveData.value = it.name
		_heightLiveDataLiveData.value = it.height
		_massLiveDataLiveData.value = it.mass
		_haircolorLiveDataLiveData.value = it.hair_color
		_skincolorLiveDataLiveData.value = it.skin_color
		_eyecolorLiveDataLiveData.value = it.eye_color
		_birthyearLiveDataLiveData.value = it.birth_year
		_genderLiveDataLiveData.value = it.gender
		_nameLiveDataLiveData.value?.let { name->
			_isFavoriteLiveData.value = favoritesRepository.checkFavoriteStatus(name)
		}
	}
}